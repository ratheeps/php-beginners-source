<?php

$host = 'localhost';
$username = 'root';
$password = 'root';
$database = 'class';
$conn = new mysqli($host, $username, $password, $database);
if ($conn->connect_error) die('Connection error : '. $conn->connect_error);
if (!$conn->connect_error){
    $sql = 'CREATE TABLE students (
        id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
        firstname VARCHAR(30) NOT NULL,
        lastname VARCHAR(30) NOT NULL,
        age INT(3) NOT NULL
        )';
    $result = $conn->query($sql);
    if ($result){
        echo 'Table created';
    }else{
       echo $conn->error;
    }
}