<?php
if ($_SERVER['REQUEST_METHOD'] == 'POST' &&  $_REQUEST['submit'] == 'Login'){
    $userName = $_POST['username'];
    $password = $_POST['password'];
    if (empty($userName)){
        header("Location: /form?error=Invalid username");
        return false;
    }
    if (empty($password)){
        header("Location: /form?error=Invalid password");
        return false;
    }
    header("Location: /form?success=Valid data");
    return true;
}