<?php
$filePath = 'data.txt';

function fileReader($filePath){
    if (file_exists($filePath)){
        $file = fopen($filePath, 'r') or die('File open error');
        $content = fread($file,  filesize($filePath));
        fclose($file);
        return $content;
    }else{
        return 'file not exists';
    }

}

function fileWriter($filePath, $text){
    $file = fopen($filePath, 'w+') or die('File open error');
    $writer = fwrite($file, $text) or die('Writing failed');
    fclose($file);
    return $writer;
}

$content = fileWriter($filePath, 'Welcome');
$content = fileReader($filePath);
echo $content;
