<?php
// String
$a = "apple";
$b = 'Orange';
var_dump($a); echo "<br/>";
var_dump($b); echo "<br/>";
echo $a; echo "<br/>";

//Numbers
$c = 10;
var_dump($c);
echo "<br/>";

//Float
$d = 1.24;
var_dump($d);
echo "<br/>";
// boolean
$e = true;
$f = false;
var_dump($e);
echo "<br/>";

// Array
$g = [1, 2, 3, 4];
$h = array(1, 2, 3, 4);
var_dump($g);
echo "<br/>";
var_dump($h);
echo "<br/>";

// Object
class apple {
    function getColor()
    {
        return "Green";
    }
}
$i = new  apple();
var_dump($i);
echo "<br/>";

// null vale
$j = null;
var_dump($j);