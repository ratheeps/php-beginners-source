<?php
$var1 = 10;
$var2 = "10";
$var3 = 20;
$var4 = 20;

// Equal (== or <> operator)
$var5 = ($var1 == $var2) ? 'true' : 'false';
echo '$var1 == $var2 = ' . $var5;
echo "<br/>";
// Identical (=== operator)
$var6 = ($var1 === $var2) ? 'true' : 'false';
echo '$var1 === $var2 = ' . $var6;
echo "<br/>";
$var7 = ($var3 === $var4) ? 'true' : 'false';
echo '$var3 === $var4 = ' . $var7;
echo "<br/>";

//Not Equal (!= operator)
$var5 = ($var1 != $var2) ? 'true' : 'false';
echo '$var1 != $var2 = ' . $var5;
echo "<br/>";

$var8 = ($var1 != $var3) ? 'true' : 'false';
echo '$var1 != $var3 = ' . $var8;
echo "<br/>";

$var8 = ($var1 <> $var3) ? 'true' : 'false';
echo '$var1 <> $var3 = ' . $var8;
echo "<br/>";

// Not identical (!== operator)

$var9 = ($var1 !== $var2) ? 'true' : 'false';
echo '$var1 !== $var2 = ' . $var9;
echo "<br/>";

$var10 = ($var1 !== $var3) ? 'true' : 'false';
echo '$var1 !== $var3 = ' . $var10;
echo "<br/>";

// Greater than  (> operator)

$var9 = ($var1 > $var2) ? 'true' : 'false';
echo '$var1 > $var2 = ' . $var9;
echo "<br/>";

$var9 = ($var3 > $var2) ? 'true' : 'false';
echo '$var3 > $var2 = ' . $var9;
echo "<br/>";

// Less than  (< operator)

$var9 = ($var1 < $var2) ? 'true' : 'false';
echo '$var1 < $var2 = ' . $var9;
echo "<br/>";

$var9 = ($var3 < $var2) ? 'true' : 'false';
echo '$var3 < $var2 = ' . $var9;
echo "<br/>";

$var9 = ($var2 < $var3) ? 'true' : 'false';
echo '$var2 < $var3 = ' . $var9;
echo "<br/>";

//Greater than or equal to (>= Operator)

$var9 = ($var1 >= $var2) ? 'true' : 'false';
echo '$var1 >= $var2 = ' . $var9;
echo "<br/>";

$var9 = ($var3 >= $var2) ? 'true' : 'false';
echo '$var3 >= $var2 = ' . $var9;
echo "<br/>";

$var9 = ($var2 >= $var3) ? 'true' : 'false';
echo '$var2 >= $var3 = ' . $var9;
echo "<br/>";


// Less than or equal to (<= operator)

$var9 = ($var1 <= $var2) ? 'true' : 'false';
echo '$var1 <= $var2 = ' . $var9;
echo "<br/>";

$var9 = ($var3 <= $var2) ? 'true' : 'false';
echo '$var3 <= $var2 = ' . $var9;
echo "<br/>";

$var9 = ($var2 <= $var3) ? 'true' : 'false';
echo '$var2 <= $var3 = ' . $var9;
echo "<br/>";