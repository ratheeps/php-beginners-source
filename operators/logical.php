<?php
$var1 = 1;
$var2 = 5;
$var3 = 1;

// And operator (and , &&)
$var4 = ($var2 == $var3 && $var3 < $var2) ? 'true' : 'false';
echo $var4;
echo "<br/>";

// Or operator (or, || )
$var5 = ($var2 == $var3 || $var3 < $var2) ? 'true' : 'false';
echo $var5;
echo "<br/>";

// xor operator
$var6 = ($var3 == $var1 xor $var3 > $var2 xor $var2 == $var1) ? 'true' : 'false';
echo $var6;
echo "<br/>";

$var8 = false;
$var9 = (!$var8) ? 'true' : 'false';
echo $var9;
echo "<br/>";