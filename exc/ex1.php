<?php
$price = 2500;
$discount = 0;

if ($price >= 2000)
    $discount = 30;
elseif ($price >= 1000)
    $discount = 20;
elseif ($price >= 500)
    $discount = 10;

$sellingPrice = $price - ($price * ($discount / 100));
echo $sellingPrice;
