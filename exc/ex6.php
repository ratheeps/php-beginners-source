<?php
$data = [10, 34, 50, 54, 67, 100, 3];

function checkNo($array){
    $evenNo = array_filter($array , 'checkEven');
    $oddNo = array_diff($array, $evenNo);
    return [
        'odd_numbers' => $oddNo,
        'even_numbers' => $evenNo
    ];
}
$numbers = checkNo($data);
print_r($numbers);

function checkEven($number){
    return ($number % 2) == 0;
}
