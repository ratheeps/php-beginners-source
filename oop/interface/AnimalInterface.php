<?php

interface AnimalInterface {
    public function setLegs($legs);
    public function getLegs();
    public function setName($name);
    public function getName();
}