<?php
include 'Animal.php';
class Dog extends Animal
{
    public function __construct($name = 'Dog', $legs)
    {
        $this->setLegs($legs);
        $this->setName($name);
    }
}