<?php
class Animal
{
    private $legs;
    private $name;

    public function setLegs($legs = 4)
    {
        $this->legs = $legs;
    }

    public function getLegs()
    {
        return $this->legs;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getName()
    {
        return $this->name;
    }
}