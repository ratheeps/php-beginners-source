<?php
include 'Animal.php';
class Cat extends Animal
{
    public function __construct($name = 'Cat', $legs)
    {
        $this->setLegs($legs);
        $this->setName($name);
    }

}