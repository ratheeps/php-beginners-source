<?php

class Student{
    public $student = [];

    public function __construct($name = null, $age = null)
    {
        $this->setAttribute('name', $name);
        $this->setAttribute('age', $age);
    }

    public function __set($name, $value)
    {
        $this->setAttribute($name, $value);
    }

    public function __get($name)
    {
        return $this->getAttribute($name);
    }

    public function __call($name, $arguments)
    {
        echo $name;
        print_r($arguments);
        return 'undefined method';
    }

    public function setAttribute($name, $value){
        $this->student[$name] = $value;
    }

    public function getAttribute($name){
        if (isset($this->student[$name])){
            return $this->student[$name];
        }
        return null;
    }

    public function get(){
        return $this->student;
    }
}