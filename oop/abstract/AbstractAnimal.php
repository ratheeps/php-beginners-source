<?php

abstract class AbstractAnimal
{
    public $legs;
    public $name;

    public function setLegs($legs){
        $this->legs = $legs;
    }
    abstract public function getName();
    abstract public function setName($name);
    abstract public function getLegs();
}