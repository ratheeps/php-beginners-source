<?php
$array1 = [10,22,33,44,55];

array_push($array1, 66);
print_r($array1);
echo '<br>';

$array2 = [11,22,33,44,55];
array_pop($array2);
print_r($array2);
echo '<br>';

$array3 = array_diff($array1, $array2);
print_r($array3);
echo '<br>';

$array4 = ['a'=>10, 'b'=>20, 'd' => 30];
$array5 = ['a'=>10, 'b'=>20, 'c' => 30, 'd' => 40];
$array6 = array_diff_assoc($array5, $array4);
print_r($array6);
echo '<br>';

$count = count($array4);
echo $count;
echo '<br>';

$index = array_search(22, $array2);
echo $index;
echo '<br>';

$keys = array_keys($array5);
print_r($keys);
echo '<br>';

$values = array_values($array5);
print_r($values);
echo '<br>';
echo '<br>';

print_r($array1);
echo '<br>';
$newArray = array_slice($array1, 1, 2);
print_r($newArray);
echo '<br>';
echo '<br>';

print_r($array1);
echo '<br>';
$newArray2 = array_splice($array1, 1, 2, $array2);
print_r($newArray2);
echo '<br>';
echo '<br>';

$newArray3 = array_reverse($array1);
print_r($newArray3);
echo '<br>';

$newArray4 = array_merge($array1, $array2);
print_r($newArray4);
echo '<br>';
echo '<br>';
echo '<br>';
$newArray5 = array_filter($array1, 'myFilterFunction');
print_r($newArray5);
function myFilterFunction($value){
    return $value >= 30;
}
echo '<br>';
echo '<br>';

$newArray6 = array_map('myMapFunction', $array1);
print_r($newArray6);
function myMapFunction($value){
    return $value * 2;
}