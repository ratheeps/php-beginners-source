<?php

/** define a array */
$array1 = ['name' => 'Kamal', 'phone' => 077777777 , 'age'=> 20];
$array2 = array('name' => 'Kamal', 'phone' => 077777777, 'age'=> 20);

/** retrieve a value */
echo $array1['name'];
echo '<br>';

/** Set value */
print_r($array2);
echo '<br>';
$array2['name'] = 'Raja';
print_r($array2);
echo '<br>';
echo '<br>';

/** ascending soring */
print_r($array2);
echo '<br>';
// sort by value
asort($array2);
print_r($array2);
echo '<br>';

// sort by key
ksort($array2);
print_r($array2);
echo '<br>';
echo '<br>';

/** descending soring */
print_r($array2);
echo '<br>';
// sort by value
rsort($array2);
print_r($array2);
echo '<br>';

// sort by key
krsort($array2);
print_r($array2);
echo '<br>';