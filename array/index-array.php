<?php

/** define a array */
$array1 = [10, 22, 30, 40];
$array2 = array(10, 22, 33, 44);

/** retrieve a value */
echo $array1[2];
echo '<br>';

/** Set value */
print_r($array2);
echo '<br>';
$array2[1] = 50;
print_r($array2);
echo '<br>';
echo '<br>';

/** ascending soring */
print_r($array2);
echo '<br>';
// sort by value
asort($array2);
print_r($array2);
echo '<br>';

// sort by key
ksort($array2);
print_r($array2);
echo '<br>';
echo '<br>';

/** descending soring */
print_r($array2);
echo '<br>';
// sort by value
rsort($array2);
print_r($array2);
echo '<br>';

// sort by key
krsort($array2);
print_r($array2);
echo '<br>';