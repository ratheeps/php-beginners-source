<?php
$array1 = [[1, 2], [4, 5, 6], [8, 9, 0], ['a', 'b'], 10];
print_r($array1[0][1]);
echo '<br/>';

$array2 = [
    'users' => [
        [
            'name' => 'Raja',
            'age' => 20
        ],
        [
            'name' => 'Vimal',
            'age' => 30
        ],
        [
            'name' => 'Kamal',
            'age' => 25
        ]
    ],
    'accounts' =>
        [
            'account1',
            'account2'
        ]
];
print_r($array2['users'][0]['name']);
echo '<br/>';