<?php

/** Function */
function printName()
{
    echo 'Kamal';
}
printName();
echo '<br/>';

/** Params */
function sum($num1, $num2)
{
    echo $num1 + $num2;
    echo '<br/>';
}
sum(10, 11);

/** default params */
function printAge($age = 30)
{
    echo $age;
}
printAge();
echo '<br/>';

/** return */
function discount($price, $dis){
    $dAmount =  $price * ($dis/100);
    return $dAmount;
}

$dAmount = discount(1500, 15);
echo $dAmount;
echo '<br/>';
$dAmount2 = discount(2500, 13);
echo $dAmount2;
echo '<br/>';