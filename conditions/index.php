<?php
// if ..
// if .. else
// if .. elseif
// if .. elseif .. else

$var1 = true;
$var2 = false;
$var2 = true;

// If conditions
if ($var1){
    echo "var1 is true";
    echo "<br/>";
}

if (!$var1){
    echo "var1 is false";
    echo "<br/>";
}

// if .. else

if ($var1){
    echo "var1 is true";
    echo "<br/>";
} else {
    echo "var1 is false";
    echo "<br/>";
}


// if .. elseif .. else
$time  = date('H');
if ($time < 12){
    echo 'GM';
}elseif ($time < 15){
    echo 'GA';
}elseif ($time < 18){
    echo 'GE';
}else{
    echo 'GN';
}